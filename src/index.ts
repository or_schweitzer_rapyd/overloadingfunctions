import log from "@ajar/marker";
import { saySomething } from "./myModule.js";

//const response = saySomething("hello");
//log.magenta(response);
/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */


function random(): number;

function random(format: boolean): number;

function random(format: boolean, max : number): number;

function random(format: boolean, min : number, max: number): number;


function random(arg1?: boolean, arg2? : number, arg3?: number) {

    let max;
    let min;
    if(arg1 === undefined) return  Math.round(Math.random());

    else if(arg1 === false && arg2 === undefined){
        //Integer format is given with no range
        return  Math.round(Math.random());
    }

    else if(arg1 === true && arg2 === undefined){
         //float format is given with no range
         return Math.random();
    }

    else if(arg2 && arg3){
        //min and max are given
        min = arg2;
        max = arg3;
        
        if(arg1 === true){
            //float format
            return min + Math.random()*(max - min)
           
        }else{
             //integer format
             return min + Math.floor(Math.random()*(max - min))
        }
       
    }
    else if(arg2 && !arg3){//only two params are given
        max  = arg2;
        if(arg1 === true){
            //float format
            return  Math.random()*(max)
        }else{
            //integer format
            return  Math.floor(Math.random()*(max))
        }

    }
    
    return -1;
}

 const num1 = random(); // 0 ~ 1 | integer
 const num2 = random(true); // 0 ~ 1 | float
 const num3 = random(false, 6); // 0 ~ 6 | integer,
 const num4 = random(false, 2, 6); // 2 ~ 6 | integer
 const num5 = random(true, 6); // 0 ~ 6 | float
 const num6 = random(true, 2, 6); // 2 ~ 6 | float

 console.log({ num1, num2, num3, num4, num5, num6 });

//---------------------------------------------------------------
